//#############################################################################
//### Filename: floppy_music.cpp
//### Author:   Scott Vincent
//### Date:     16 Feb 2014
//### Site:     https://www.raspberrypi.org/forums/viewtopic.php?f=41&t=69947
//#############################################################################
//### HW Setup: Floppy 18--> RPi pin 11:GPIO0  Motor Direction
//###           Floppy 20--> RPi pin 12:GPIO1  Moror Step
//###			Floppy 19--> RPi pin 6,9,14:Ground
//###			Floppy 21--> RPi pin 6,9,14:Ground
//### SW Setup: Compile cmd: g++ -o fm floppy_music.cpp -lwiringPi
//### 			Alt Compilr: gcc -o fm floppy_music.c -lwiringPi
//###			TerminalCmd: ./fm
//#############################################################################
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>

const int dirPin = 0;			//pin 11 = wiringPi Pin 0: Motor direction.
const int stepPin = 1;		   	//pin 12 = wiringPi Pin 1: Stepper motor.
enum { Cn, Cs, Dn, Ds, En, Fn, Fs, Gn, Gs, An, As, Bn, Zz };	   //Octave: naturals and sharps (Zz = rest)
enum { Bs, Df, Dn2, Ef, En2, Es, Gf, Gn2, Af, An2, Bf, Bn2, Zz2 }; //Octave:flats and remaining sharps

// Frequencies in 1/100th of Hz. Middle A = 44000
// 4 Octaves: 3,4,5,6
// 12 notes per octave. C,C#,D,D#,E,F,F#,G,G#,A,A#,B
const int freq[4][12] = {
	{ 13081,13859,14683,15556,16481,17461,18500,19600,20765,22000,23308,24694 },
	{ 26163,27718,29366,31113,32963,34923,36999,39200,41530,44000,46616,49388 },
	{ 52325,55437,58733,62225,65925,69846,73999,78399,83061,88000,93233,98777 },
	{ 104650,110873,117466,124451,131851,139691,147998,156798,166122,176000,186466,197553 }
};

/* Frequency(Hz) is converted to Floppy Delay: 314000/frequency = floppy delay
 *    middle A = 314000/440 = 714
 *    Lowest realistic note is delay = 1550
 *    Highest realistic note is delay = 210    */
const int floppyConv = 31400000;
int floppyDelay[4][12];			// Calculate all our floppy delays at the start

//##############################################
//### Song1: Star Wars Theme
//### https://www.musicnotes.com/sheetmusic/mtd.asp?ppn=MN0127456
//### Use:   Note, octave, length
//##############################################
const int song1_tempo = 140 * 8;
const int song1[][3] = {
	{Zz, 1, 8},
	{Dn, 1, 8},
	{Dn, 1, 8},
	{Dn, 1, 8},

	{Gn, 1,16},
	{Dn, 2,16},

	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 2,16},
	{Dn, 2, 8},

	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 2,16},
	{Dn, 2, 8},

	{Cn, 1, 4},  //Bar5
	{Bn, 1, 4},
	{Cn, 1, 4},
	{An, 1,16},
	{Dn, 1, 4},
	{Dn, 1, 4},
	
	{Gn, 1,16},
	{Dn, 2,16},

	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 2,16},
	{Dn, 2, 8},

	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 2,16},
	{Dn, 2, 8},

	{Cn, 1, 4},
	{Bn, 1, 4},
	{Cn, 1, 4},
	{An, 1,16},
	{Dn, 1, 4},
	{Dn, 1, 4},
	
	{En, 1,12},  //Bar10
	{En, 1, 4},
	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 1, 4},
	
	{Gn, 1, 4},  //Bar11
	{An, 1, 4},
	{Bn, 1, 4},
	{An, 1, 8},
	{En, 1, 4},
	{Fs, 1, 8},
	{Dn, 1, 8},
	{Dn, 1, 4},	
	
	{En, 1,12},  //Bar12
	{En, 1, 4},
	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 1, 4},

	{Dn, 2, 8},  //Bar13
	{An, 1,16},
	{Dn, 1, 8},
	{Dn, 1, 8},
	
	{En, 1,12},  //Bar14 page2
	{En, 1, 4},
	{Cn, 1, 4},
	{Bn, 1, 4},
	{An, 1, 4},
	{Gn, 1, 4},
	
	{Gn, 1, 4},  //Bar15
	{An, 1, 4},
	{Bn, 1, 4},
	{An, 1, 8},
	{En, 1, 4},
	{Fs, 1, 8},
	{Dn, 1, 8},
	{Dn, 1, 4},	
	
	{Gn, 2, 4},	
	{Fn, 2, 4},	
	{Ef, 2, 4},	
	{Dn, 1, 4},	
	{Cn, 1, 4},	
	{Bf, 1, 4},	
	{An, 1, 4},	
	{Gn, 1, 4},	
	
	{Dn, 2,16},	
	{Zz, 1, 4},	
	{Dn, 1, 4},	
	{Dn, 1, 4},	
	{Dn, 1, 4},	
	
	{Gn, 1,16},	
	{Dn, 2,16},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{An, 1, 4},	
	{Gn, 2,16},	
	{Dn, 2, 8},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{An, 1, 4},	
	{Gn, 2,16},	
	{Dn, 2, 8},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{Cn, 1, 4},	
	{An, 1,16},	
	{Dn, 1, 8},	
	{Dn, 1, 4},	

	{Gn, 1,16},	
	{Dn, 2,16},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{An, 1, 4},	
	{Gn, 2,16},	
	{Dn, 2, 8},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{An, 1, 4},	
	{Gn, 2,16},	
	{Dn, 2, 8},	

	{Cn, 1, 4},	
	{Bn, 1, 4},	
	{Cn, 1, 4},	
	{An, 1,16},	
	{Dn, 1, 8},	
	{Dn, 1, 4},	

	{-1,-1,-1}
};


static void resetMotor()
{
	// To reset head position move back 10 then forward 5
	digitalWrite(dirPin, LOW);
	for (int i=0; i < 10; i++){
		digitalWrite(stepPin, HIGH);
		digitalWrite(stepPin, LOW);
		delay(1);
	}
	digitalWrite(dirPin, HIGH);
	for (int i=0; i < 5; i++){
		digitalWrite(stepPin, HIGH);
		digitalWrite(stepPin, LOW);
		delay(1);
	}
	delay(400);
}

static int init()
{
	if (wiringPiSetup() == -1){
		printf("Failed to initialize wiringPi\n");
		return 1;
	}

	pinMode(stepPin, OUTPUT);
	pinMode(dirPin, OUTPUT);

	resetMotor();

	for (int octave = 0; octave < 4; octave++){
		for (int note = 0; note < 12; note++){
			floppyDelay[octave][note] = floppyConv / freq[octave][note];
		}
	}

	return 0;
}

static void playNote(int note, int octave, int length)
{
	static int dir = 1;
	int pause = floppyDelay[octave][note] * 10;

	int endTime = millis() + length;
	while (millis() < endTime){
		digitalWrite(dirPin, dir);
		if (dir == 0)
			dir = 1;
		else
			dir = 0;

		digitalWrite(stepPin, HIGH);
		digitalWrite(stepPin, LOW);
		delayMicroseconds(pause);
	}
}

static void rest(int length)
{
	int endTime = millis() + length;
	while (millis() < endTime){
		delay(5);
	}
}

static void playSong(const int song[][3], const int tempo)
{
	// song[note_num][note, octave, length]
	// Convert tempo in BPM to millisecs
	int noteLen = 60000 / tempo;

	for (int i = 0; song[i][0] != -1; i++){
		int length = song[i][2] * noteLen;
		if (song[i][0] == Zz){
			rest(length);
		}
		else {
			playNote(song[i][0], song[i][1], (7 * length) / 8);
			rest(length / 8);
		}
	}
}

int main()
{
	if (init() != 0){
		printf("init failed - Exiting\n");
		return 1;
	}
	playSong(song1, song1_tempo);
	playSong(song1, song1_tempo);
	playSong(song1, song1_tempo);
	//playSong(song2, song2_tempo);
	return 0;
}
