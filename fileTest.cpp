//#############################################################################
//### Filename: fileTest.cpp
//### Author:   Martin Lim
//### Date:     25 Jan 2018
//### Web: Read .csv file in C Stackoverflow
//### 	   https://stackoverflow.com/questions/12911299/read-csv-file-in-c
//#############################################################################
//### SW Setup: Compile cmd: g++ -o fileTest fileTest.cpp 
//### 		Alt Compilr: gcc -o fileTest fileTest.c 
//###		TerminalCmd: ./fileTest

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int song[][3] = {
	{ 0, 1, 2 },
	{ 1, 2, 4 },
	{ 0, 3, 6 },
	{ 1, 4, 8 },
	{ 0, 5, 10 },
	{ 1, 6, 12 },
	{ 0, 7, 14 },
	{ 1, 8, 16 },
	{ -1, -1, -1 }
};

const char* getfield(char* line, int num)
{
    const char* tok;
    for (tok = strtok(line, ",");
            tok && *tok;
            tok = strtok(NULL, ",\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}

void printSong()
{
	for (int i = 0; song[i][0] != -1; i++){
		printf("PrintSong%d:%d\n",i,song[i][1]);
	}
}

int main()
{
    FILE* stream = fopen("fileTest.txt", "r");
    char line[1024];
	int songIn[100][3];
    int i=0;
    
    while (fgets(line, 1024, stream))
    {
        char* tmp = strdup(line);
        
        //****DEBUG*****
		//int asdf=0;
        printf("%s",getfield(tmp, 3));
		//printf("%zu\n", sizeof(getfield(tmp, 1))); 
        //asdf = atoi(getfield(tmp, 2));
        //printf("%d",asdf);
        //****DEBUG*****

        songIn[i][0]= atoi(getfield(tmp, 1));
        //songIn[i][1]= atoi(getfield(tmp, 2));
        //songIn[i][2]= atoi(getfield(tmp, 3));
        printf("Song%d:%d,%d,%d\n",i,songIn[i][0],songIn[i][1],songIn[i][2]);
       
        // NOTE strtok clobbers tmp
        free(tmp);
        i++;
    }
}
